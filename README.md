**MAP4CNS**
===========
Map reads and perform DAG-based consensus using distributed computing.

DESCRIPTION
===========
In contrast to long-read assemblers employing the more-traditional strategy of consensus-based read-read error correction prior to OLC assembly, a number of state-of-the-art long-read *de novo* genome assemblers (such as [DBG2OLC](https://github.com/yechengxi/DBG2OLC) and [MINIASM](https://github.com/lh3/miniasm])) assemble long, noisy reads directly. The resulting backbone contigs maintain a 15% error rate and require "polishing" to remove these errors and other large sequencing artifacts.

`map4cns` is a Bash script coordinating the distributed parallelization of [pbdagcon](https://github.com/PacificBiosciences/pbdagcon) over an SGE/UGE computing grid to polish backbone sequences. `map4cns` takes as input an FOFN file of long-read [FASTA](https://en.wikipedia.org/wiki/FASTA_format) files and a backbone genome sequence (also in FASTA format). First, the raw long-read data are aligned to the input backbone sequence, generating a single [BAM](https://samtools.github.io/hts-specs/SAMv1.pdf) file. The backbone sequences are virtually balanced into a user-defined number of bins and then `pbdagcon` is run on each backbone sequence independently on the grid. Depth of coverage is also computed. Finally, these distributed files are then merged into single output consensus FASTA and raw-read depth files.

This pipeline also provides a mechanism for running jobs in parallel on a single many-core machine. `map4cns` uses touch files to track the progression of the pipeline, so only one instance of `map4cns` should be run within a project directory at one time. 

USAGE
=====
```
  map4cns --config <config> [--stop-after <stage>]
         [--redo-from <stage>] [--cleanup]
```

OPTIONS
=======

Command line options
--------------------
```
  --config <config>
     File; required. The configuration file parameterizing a map4cns run.
     The config specifies, at a minimum, the BACKBONE_FASTA, READS_FOFN,
     and WORKDIR variables. See `examples/map4cns.config' for details.
     Default: Null

  --redo-from <stage>
     String; optional. Force redo all stages starting from (and including)
     the specified stage. Valid stages are: faidx, saidx, bwtidx, blasr,
     bwa, sort, merge, stats, balance, consensus, gather
     Default: Null

  --stop-after <stage>
     String; optional. Stop the pipeline after the specified stage is
     completed. See `--redo-from' option for a list of valid stages.
     Default: Null

  --cleanup
     Flag; optional. Remove intermediate files in completed project stage
     subdirectories.
     Default: false
```

Configuration file
------------------
`map4cns` uses a configuration file to parameterize the various tools coordinated by the pipeline and to request computing resources. An example config file with reasonable executable settings is in [`example/map4cns.config`](example/map4cns.config). It's parameters and descriptions are:

```
# Pipeline inputs (required):
  BACKBONE_FASTA=<file>
    The raw backbone contig sequences in FASTA format outputted from a
    long-read assembler.

  READS_FOFN=<file>
    A File-Of-FileNames listing the uncompressed FASTA/FASTQ files to
    use for error correction (see examples/example_*.fofn).

# Pipeline outputs (required):
  WORKDIR=<str> 
    (str) The output file path of the working directory from where the
    map4cns pipeline will run and organize outputs. Also used as file-
    name prefixes (cannot be an empty string or `.'). Directories are
    created by map4cns if needed.

# Runtime parameters (optional)
  NUM_PARTS=[uint]
    The number of partitions to balance the backbone contigs into
    for parallelized error correction with pbdagcon.

  USE_BWA=[bool] {unset|0|1}
    Align reads with BWA-MEM (Blasr by default). This is recommended
    when genome size is greater than 4Gb in size.

  READS_PAIRED=[bool] {unset|0|1}
    The input reads are paired-end and the read-ends are separated into
    R1 and R2 files (force USE_BWA=1).

  READS_COLLATED=[bool] {unset|0|1}
    The input reads are paired-end and collated/interleaved into a 
    single file (force READS_PAIRED=1)

# Executable/tool options (optional):
  BLASR_EXE_OPTIONS=[str]
    Parameters to be passed to Blasr (see Blasr --help for more info)

  BWA_EXE_OPTIONS=[str]
    Parameters to be passed to BWA-MEM (see bwa mem --help for more info)

  FILTER_EXE_OPTIONS=[str]
    Parameters to be passed to bamfilter. This stage filters out secon-
    dary hits in the blasr- or BWA-generated BAM files, except hits 
    within --dist=<int> bp of the contig termini. This parameter was 
    introduced to improve consensus accuracy at the termini of the 
    contigs when pairs of contigs are suspected of end overlap. (see
    bamfilter --help for more info)

  SAMSORT_EXE_OPTIONS=[str]
    Parameters to be passed to samtools sort (see samtools sort --help
    for more info)

  SAMMERGE_EXE_OPTIONS=[str] 
    Parameters to be passed to samtools merge (see samtools merge --help
    for more info)

  CONSENSUS_EXE_OPTIONS=[str]
    Parameters to be passed to pbdagcon (see pbdagcon --help for more info)

# Parallelization parameters (optional):
  BLASR_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the blasr step (see 
    qbatch submit --help for more info) 

  BWA_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the bwa step (see 
    qbatch submit --help for more info) 

  FILTER_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the filter step (see 
    qbatch submit --help for more info) 

  SAMSORT_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the sort step (see 
    qbatch submit --help for more info) 

  SAMMERGE_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the samtools merge step (see 
    qbatch submit --help for more info) 

  STATS_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the stats step (see 
    qbatch submit --help for more info) 

  CONSENSUS_SUBMIT_OPTIONS=[str]
    Parameters to be passed to qbatch at the consensus step (see 
    qbatch submit --help for more info)
```

>For parameters expecting file names or strings, values containing spaces must be quoted. 
>When intending to set a boolean parameter to false, it is safer to be left unset (*e.g.*, `READS_PAIRED=;`) than to be set to `0`.

INSTALL
=======
Dependencies
------------
`map4cns` has the following external dependencies (which have their own dependencies):

- **Make**
- **gcc** v4.9+ (required by the tools below)
- **SGE/PBS/SLURM** (or similar; optional, but recommended)
- [**Git**](https://git-scm.com/downloads) v1.8+
- [**Perl**](https://www.perl.org/get.html) v5.10+
- [**Python**](https://www.python.org/downloads/) v2.7+ (requires pysam)
- [**SAMtools**](https://github.com/samtools/samtools) v1.0+
- [**BEDtools**](https://github.com/arq5x/bedtools2)
- [**BWA**](https://github.com/lh3/bwa) v0.7+
- [**Blasr**](https://github.com/PacificBiosciences/blasr) v5.1+ (optional, but recommended)
- [**pbdagcon**](https://github.com/PacificBiosciences/pbdagcon)

>Note: On MacOS/OSX systems, [CommandLine Tools for Xcode](https://developer.apple.com/download/more/) is required. These tools are available free of charge with an Apple ID. 

Once all dependencies have been properly installed, all source paths must be accessible via the `$PATH` environment variable.  

Installation
------------
Installing `map4cns` requires an internet connection and Git. Issue the following commands on the commandline:
```bash
git clone --recursive git@bitbucket.org:rokhsar-lab/map4cns.git
cd map4cns
make
make install PREFIX=$PWD
source ./activate
```
This will download and install some libraries into a `lib` subdirectory within the `map4cns` directory. Now you are good-to-go.


BUGS & LIMITATIONS
==================
Known Issues
------------
Blasr's `sawriter` tool cannot index genomes 4 Gb or larger. Some pre-partitioning may be required or USE_BWA instead.

The code for interacting with a grid scheduler has been configured to run in the [NERSC](http://www.nersc.gov) computing environment and has not been tested on other systems.

Reporting
---------
All feature requests and bug reports should be directed to the author.

AUTHOR
=======
Jessen V. Bredeson <jessenbredeson@berkeley.edu>

TODO
====
- In blasr step: add filter to allow two mappings for reads mapped to the first or last ~15kb of a contig.
- In pbec step: remap reads to corrected sequence, calculate depth begraph, lower-case mask untrusted regions.
- In merge step: merge corrected fasta, reads remapped to corrected fasta, and depth bedgraph by input order.


COPYRIGHT
=========
Copyright (c)2020. The Regents of the University of California (Regents). All Rights Reserved. Permission to use, copy, modify, and distribute this software and its documentation for educational, research, and not-for-profit purposes, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright notice, this paragraph and the following two paragraphs appear in all copies, modifications, and distributions. Contact The Office of Technology Licensing, UC Berkeley, 2150 Shattuck Avenue, Suite 510, Berkeley, CA 94720-1620, (510) 643-7201, for commercial licensing opportunities.

IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
