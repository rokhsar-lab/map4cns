
PREFIX    := /usr/local

PKG_DIR   := $(PWD)
SRC_DIR   := $(PKG_DIR)/src
SUB_DIR   := $(PKG_DIR)/submodules
BUILD_DIR := $(PKG_DIR)/build
BIN_DIR   := $(BUILD_DIR)/bin
LIB_DIR   := $(BUILD_DIR)/lib

ECHO      := echo
PYTHON    := $(filter /%,$(shell /bin/sh -c 'type python'))
INSTALL   := $(filter /%,$(shell /bin/sh -c 'type install'))
MKDIR     := $(filter /%,$(shell /bin/sh -c 'type mkdir'))
FIND      := $(filter /%,$(shell /bin/sh -c 'type find'))
CAT       := $(filter /%,$(shell /bin/sh -c 'type cat'))
SED       := $(filter /%,$(shell /bin/sh -c 'type sed'))
GIT       := $(filter /%,$(shell /bin/sh -c 'type git'))
RM        := $(filter /%,$(shell /bin/sh -c 'type rm'))

INSTALL_DIR = $(INSTALL) -m 755 -d
INSTALL_EXE = $(INSTALL) -m 755 -p 
INSTALL_LIB = $(INSTALL) -m 644 -p
MKDIR_P     = $(MKDIR) -p
RM_R        = $(RM) -r

PACKAGE   := MAP4CNS
VERSION   := $(shell $(GIT) describe --always --tags --long)
CONTACT   := https:\/\/bitbucket.org\/rokhsar-lab\/map4cns\/issues
LICENSE   := LICENSE

BIN_TARGETS = \
	$(BIN_DIR)/map4cns \
	$(BIN_DIR)/balanceFasta \
	$(BIN_DIR)/bamfilter \
	$(BIN_DIR)/bamtom5 \
	$(BIN_DIR)/ilec \
	$(BIN_DIR)/pbec \
	$(BIN_DIR)/qbatch

LIB_TARGETS = \
	$(LIB_DIR)/intervals.py

.SUFFIXES: .pl .py .sh

.PHONY: all modules info build valkyr install clean

all: $(BIN_DIR) $(BIN_TARGETS) $(LIB_DIR) $(LIB_TARGETS)

modules: $(LIB_DIR) valkyr activate

$(BUILD_DIR):
	$(MKDIR_P) $@

$(BIN_DIR): 
	$(MKDIR_P) $@

$(LIB_DIR): 
	$(MKDIR_P) $@

$(BIN_DIR)/%: $(SRC_DIR)/%.py
	-$(ECHO) '#!/usr/bin/env python' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(BIN_DIR)/%: $(SRC_DIR)/%.pl
	-$(ECHO) '#!/usr/bin/env perl' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(BIN_DIR)/%: $(SRC_DIR)/%.sh
	-$(ECHO) '#!/usr/bin/env bash' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(BIN_DIR)/%: $(SRC_DIR)/%.R
	-$(ECHO) '#!/usr/bin/env Rscript' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(LIB_DIR)/%: $(SRC_DIR)/%
	-$(CAT) $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(LIB_DIR)/Valkyr: $(LIB_DIR)
	git submodule update --init --recursive $(SUB_DIR)/Valkyr

valkyr: $(LIB_DIR)/Valkyr

info:
	@printf "\nPlease execute the following command prior to running"
	@printf "\nmap4cns or any of its associated tools and modules:"
	@printf "\n\n    source %s/activate" $(PKG_DIR)
	@printf "\n\n"

activate: $(LIB_DIR)
	@echo 'export PATH=$(PREFIX)/bin:$$PATH;' >activate; 
	@echo 'export PERL5LIB=$(PREFIX)/lib:$$PERL5LIB;' >>activate; 
	@echo 'export PYTHONPATH=$(PREFIX)/lib:$$PYTHONPATH;' >>activate; 

distclean: clean
	-$(RM) activate

clean: $(BUILD_DIR)
	-$(RM_R) $(BUILD_DIR)

install: all activate info
	$(INSTALL_DIR) $(PREFIX)/bin
	$(INSTALL_DIR) $(PREFIX)/lib
	$(INSTALL_EXE) $(BIN_DIR)/* $(PREFIX)/bin
	$(INSTALL_LIB) $(LIB_DIR)/* $(PREFIX)/lib
	$(shell cd $(SUB_DIR); $(FIND) Valkyr -type d -exec $(INSTALL_DIR) $(PREFIX)/lib/{} \;)
	$(shell cd $(SUB_DIR); $(FIND) Valkyr -name "*.pm" -exec $(INSTALL_LIB) {} $(PREFIX)/lib/{} \;)

