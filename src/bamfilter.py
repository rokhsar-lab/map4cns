
import os
import sys
import pysam
import getopt

__author__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Filter BAM files of long reads'


num = len

def filtered(records, primary, length, max_dist, clear_regions={}):
    if primary.reference_name in clear_regions:
        beg = clear_regions[primary.reference_name][0]
        end = clear_regions[primary.reference_name][1]
    else:
        beg = max_dist
        end = length[primary.reference_name] - max_dist
        
    if primary.reference_start > max_dist and \
       primary.reference_end < length[primary.reference_name] - max_dist:
        return [primary]

    for record in records:
        if record.reference_name == primary.reference_name:
            continue

        if record.reference_name in clear_regions:
            beg = clear_regions[record.reference_name][0]
            end = clear_regions[record.reference_name][1]
        else:
            beg = max_dist
            end = length[record.reference_name] - max_dist

        if record.reference_start < beg or record.reference_end > end:
            return [primary, record]

    return [primary]



def usage(message=None, status=1):
    message = '' if message is None else "\nError: %s\n\n" % message
    sys.stderr.write("\n")
    sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
    sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
    sys.stderr.write("Contact: %s\n" % __contact__)
    sys.stderr.write("\n")
    sys.stderr.write("Usage:   %s [-d,--dist <int>] [-c,--clear <file>] <in.bam> <out.bam>\n%s" % (
        __program__, message))
    sys.exit(status)



def main(argv):
    max_dist = 0
    clearfile = None    
    options, arguments = getopt.getopt(argv, 'hc:d:', ('help','dist=','clear='))
    for flag, value in options:
        if   flag in {'-h', '--help'}: usage()
        elif flag in {'-d', '--dist'}: max_dist = int(value)
        elif flag in {'-c', '--clear'}: clearfile = value
        
    if len(arguments) != 2:
        usage("Unexpected number of arguments")

    ibam = pysam.AlignmentFile(arguments[0])
    obam = pysam.AlignmentFile(arguments[1], mode='wb', template=ibam)

    reference_length = { sq['SN'] : int(sq['LN']) for sq in ibam.header['SQ'] }    

    clear = {}
    if clearfile is not None:
        clearfile = open(clearfile ,'r')
        for line in clearfile:
            fields = line.strip().split('\t')
            clear[field[1]] = (int(field[1]), int(field[2]))

        clearfile.close()

        
    records = []
    primary = None
    prev_query_name = None
    for curr in ibam:
        if curr.is_unmapped or \
           curr.reference_id < 0:
            continue

        if curr.query_name != prev_query_name:
            if num(records) > 0:
                for record in filtered(records, primary, reference_length, max_dist, clear):
                    obam.write(record)

            primary = None            
            records = []

        if ((curr.flag & 0x900) == 0):
            primary = curr

        records.append(curr)
        prev_query_name = curr.query_name

    if num(records) > 0:
        for record in filtered(records, primary, reference_length, max_dist, clear):
            obam.write(record)

    return 0



if __name__ == '__main__':
    main(sys.argv[1:])


