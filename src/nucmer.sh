
raw=$1;
cns=$2;
out=$3;

mkdir -p nucmer;

first=1;
cut -f1 $raw.fai | while read contig; do
    samtools faidx $raw "$contig" >nucmer/raw.fa;
    samtools faidx $cns "$contig" >nucmer/cns.fa;
    printf "[nucmer] aligning $contig\n" >&2;
    nucmer -p nucmer/out nucmer/raw.fa nucmer/cns.fa 2>nucmer/out.log;
    if [ -n "$first" ]; then
	printf "$raw $cns\n" >$out.delta;
	printf "NUCMER\n" >>$out.delta;
	tail -n +3 nucmer/out.delta >>$out.delta;
	first="";
    else
	tail -n +3 nucmer/out.delta >>$out.delta;
    fi
done

rm -r nucmer

printf "[nucmer] done\n" >&2;

