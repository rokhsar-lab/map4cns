
import os
import sys
import bisect

__author__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_NAME__-__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Provide genomic interval searching support for __PACKAGE_NAME__'


num = len

class IntervalList(object):
    def __init__(self, intervals=[], setter=lambda x: x, getter=lambda x: x):
        self.__set = setter
        self.__get = getter
        self.__len = 0  # an optimization
        self.intervals = []
        for interval in intervals:
            self.add(interval)

            
    def __getitem__(self, item):
        return self.__get(self.intervals[item])

    
    def __delitem__(self, item):
        del(self.intervals[item])
        self.__len = num(self.intervals)

        
    def __iter__(self):
        return iter(map(self.__get, self.intervals))

    
    def __len__(self):
        return self.__len

    
    @property
    def is_empty(self):
        return self.__len == 0

    
    def add(self, interval):
        other = self.__set(interval)
        
        if self.is_empty:
            self.intervals.append(other)
            
        else:
            # Using bisect.insort_right, all exact-match entries
            # will be inserted _after_ their match in the list;
            # bisect.insort_left would insert _before_
            bisect.insort_right(self.intervals, other)

        self.__len += 1
            
        return True

    
    def generic_search(self, interval, left=False):
        if left:
            return bisect.bisect_left(self.intervals, self.__set(interval))
        else:
            return bisect.bisect_right(self.intervals, self.__set(interval))

    
    def overlap_search(self, other):
        beg = 0
        end = num(self) - 1

        if beg > end:
            return -1
    
        while beg <= end:
            mid = (beg + end) // 2

            this = self[mid]
            
            if other.overlaps(this):
                # Our segment overlaps
                # another placed segment
                return mid

            elif other.beg < this.beg:
                # This mid is downstream, move it up
                end = mid - 1

            elif other.end > this.end:
                # This mid is upstream, move it down
                beg = mid + 1
                
            else:
                raise NotImplementedError("Unhandled condition (%d -- %d -- %d)\n" % (beg, mid, end))

        return -1


    def overlap_range(self, other, index=None):
        """
        Returns a tuple containing the Pythonic 
        slice range of overlapping intervals
        """
        if index is None:
            index = self.overlap_search(other)
        if index < 0:
            return (index, index)

        b = index
        e = index
        L = num(self) - 1
        while b > 1:
            if not self[b-1].overlaps(other):
                break
            b -= 1

        while e < L:
            if not self[e+1].overlaps(other):
                break
            e += 1
        
        return (b, e+1)
        

    def overlap_fraction(self, other, index=None):
        """
        Returns the fraction of overlap a query 
        Interval oject has with the IntervalList
        """
        if index is None:
            index = self.overlap_search(other)
        if index < 0:
            return 0.0

        overlap_fraction = other.overlap_fraction(self[index])
        
        b = index
        e = index
        L = num(self) - 1
        while b > 1:
            this = self[b-1]
            if not this.overlaps(other):
                break
            overlap_fraction += other.overlap_fraction(this)
            b -= 1

        while e < L:
            this = self[e+1]
            if not this.overlaps(other):
                break
            overlap_fraction += other.overlap_fraction(this)
            e += 1
    
        # can be >1.0 if the neighbors were overlapping
        return overlap_fraction

    
class Strand(object):
    def __init__(self, obj):
        if isinstance(obj, Strand):
            self.__strand = obj.__strand
        elif isinstance(obj, int):
            self.__strand = obj // abs(obj) if obj else obj
        elif isinstance(obj, str):
            self.__strand = self._to_int(obj)
        else:
            raise TypeError("Invalid strand type: int, str, or Strand obj expected")

    def _to_int(self, strand):
        if   strand == '.':
            return 0
        elif strand == '-':
            return -1
        elif strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value: `+', `-', or `.' expected")
        
    def __str__(self):
        if self.__strand == 0:
            return '.'
        if self.__strand < 0:
            return '-'
        if self.__strand > 0:
            return '+'

    @property
    def int(self):
        return self.__strand

    @property
    def str(self):
        return str(self)
    
    def reverse(self):
        self.__strand *= -1

        
class Interval(object):
    def __init__(self, name, beg, end):
        self.name   = str(name)
        self.beg    = int(beg)
        self.end    = int(end)
        
    def __str__(self):
        return "%s:%d-%d" % (
            self.name, self.beg, self.end)

    string = __str__
    
    def overlaps(self, other):
        """test whether self has any kind of overlap with other"""
        if other.name == self.name:
            if other.beg < self.end <= other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                # (b/c self.beg < self.end, also True for 'contained' queries)
                return True

            elif self.beg < other.end <= self.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                # (b/c other.beg < other.end, also True for 'contains' queries)
                return True

        return False
    
    def contains(self, other):
        """test whether self contains other"""
        if other.name == self.name:
            if self.beg <= other.beg and other.end <= self.end:
                return True

        return False

    def within(self, other):
        """test whether self is contained within other"""
        if other.name == self.name:
            if other.beg <= self.beg and self.end <= other.end:
                return True

        return False

    def overlaps_beg(self, other):
        """test whether self overlaps other.beg"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.end < other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                return True

        return False

    def overlaps_end(self, other):
        """test whether self overlaps other.end"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.beg < other.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                return True

        return False

    def overlap_fraction(self, other):
        if not self.overlaps(other):
            return 0.0
            
        elif self.within(other):
            return 1.0
        
        elif self.contains(other):
            return float(other.end - other.beg) / self.aligned_length
            
        elif self.overlaps_beg(other):
            return float(self.end - other.beg) / self.aligned_length

        elif self.overlaps_end(other):
            return float(other.end - self.beg) / self.aligned_length

        else:
            raise NotImplementedError("Unexpected overlap type")



