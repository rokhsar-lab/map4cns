
PROGRAM=`basename $0`;
AUTHORS='Jessen V. Bredeson'
PACKAGE='__PACKAGE_NAME__'
CONTACT='__PACKAGE_CONTACT__'
VERSION='__PACKAGE_VERSION__'
PURPOSE='PacBio-based Error Correction'

set -o pipefail

# module load blasr
# module load bedtools
# module load samtools/1.3.1
# module unload python
# module load python/2.7.10
# source ~bredeson/tools/python2.7.10/bin/activate


function log_info () { 
    printf "%s %s INFO  [%s] $1" `date '+%F %H:%M:%S,%3N'` $PROGRAM >>/dev/fd/2;
}

function log_warn () { 
    printf "%s %s WARN  [%s] $1" `date '+%F %H:%M:%S,%3N'` $PROGRAM >>/dev/fd/2;
}

function log_error () {
    printf "%s %s ERROR [%s] $2.\n\n" `date '+%F %H:%M:%S,%3N'` $PROGRAM >>/dev/fd/2;
    exit $1;
}

function error () {
    printf "Error: $2.\n\n" >>/dev/fd/2;
    exit $1;
}

function log_depend () { 
    if [[ ! -e "$1" ]]; then
	log_error 2 "Missing dependency ($1), stage did not complete.\n";
    fi
}

function abspath () {
    if [[ -z "$1" ]]; then
        log_error 255 "No var passed to abspath()";
    fi

    var=\$"$1";
    val=`eval "expr \"$var\" "`;
    if [[ -z "$val" ]]; then
        log_error 255 "Bad path, path is empty."

    elif [[ `echo "$val" | grep '^\/'` ]]; then
        eval "$1=\"$val\"";
    
    else
        if [[ "$val" == "." || "$val" == "./" ]]; then
            eval "$1=\"$PWD\"";

        elif [[ `echo "$val" | grep '^\.\/'` ]]; then
            val=`echo "$val" | sed 's/^\.\/*//'`;
            eval "$1=\"$PWD/$val\"";

        else
            eval "$1=\"$PWD/$val\"";
        
        fi
    fi
}

function usage () {
    printf "\n" >>/dev/fd/2;
    printf "Program: $PROGRAM ($PURPOSE)\n" >>/dev/fd/2;
    printf "Version: $PACKAGE $VERSION\n" >>/dev/fd/2;
    printf "Contact: $CONTACT\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Usage:   $PROGRAM <in.bam> <backbone.fa> <in.bed> <out_prefix>\n\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
}

if [[ -z "$4" ]]; then
    usage;
    error 1 "Too few arguments";
fi

BAM=$1;
REF=$2;
BED=$3;
OUT=$4;

if [[ -z "${CONSENSUS_EXE_OPTIONS}" ]]; then
    CONSENSUS_EXE_OPTIONS='-v -j 1 -t 0 -c 0';
fi

PYTHON=`which python`;
BAMTOM5=`which bamtom5`;
SAMTOOLS=`which samtools`;
BEDTOOLS=`which bedtools`;
PBDAGCON=`which pbdagcon`;
# check for accessibility of tools and presence of inputs
if [[ -z "${SAMTOOLS}" || ! -x "${SAMTOOLS}" ]]; then
    error 127 "samtools not in PATH env variable or is not executable";

elif [[ -z "${BEDTOOLS}" || ! -x "${BEDTOOLS}" ]]; then
    error 127 "bedtools not in PATH env variable or is not executable";

elif [[ -z "${PYTHON}" || ! -x "${PYTHON}" ]]; then
    error 127 "python not in PATH env variable or is not executable";

elif [[ -z "${BAMTOM5}" || ! -x "${BAMTOM5}" ]]; then
    error 127 "bamtom5 not in PATH env variable or is not executable";

elif [[ -z "${PBDAGCON}" || ! -x "${PBDAGCON}" ]]; then
    error 127 "pbdagcon not in PATH env variable or is not executable";

fi

abspath BAM;
abspath REF;
abspath BED;
abspath OUT;

log_info "Starting\n"
log_info "Version:  $VERSION\n";
log_info "Argument: ${BAM}\n";
log_info "Argument: ${REF}\n";
log_info "Argument: ${BED}\n";
log_info "Argument: ${OUT}\n";

BASE=`basename ${BED} .bed`;
WD=${PWD};


log_info "tmpdir: ${TMPDIR}/${BASE}\n";

if [[ -d "${TMPDIR}/${BASE}" ]]; then
    rm -r ${TMPDIR}/${BASE};
fi

mkdir -p ${TMPDIR}/${BASE}/cns;
mkdir -p ${TMPDIR}/${BASE}/dp;

cp -pfL ${REF} ${REF}.fai ${BED} ${TMPDIR}/${BASE}/;

cd ${TMPDIR}/${BASE}/;

REF=`basename ${REF}`;
BED=`basename ${BED}`;
out="";

i=1;
N=`wc -l ${BED} | cut -d' ' -f1`;
grep -v '^#' ${BED} | cut -f1-3 | sort -k1,1 -k2,2n -k3,3n | while read contig start end; do
    length=$(( $end - $start ));
    I=`printf %04d $i`;

    log_info "Staging for consensus for $contig:$start-$end\n";

    samtools faidx $REF "$contig:$start-$end" | tr ':' ' ' >| $I.bb.fa && \
    samtools faidx $I.bb.fa && \
	touch $I.bb.fa.complete;

    log_depend $I.bb.fa.complete;


    samtools view -b $BAM "$contig:$start-$end" >| $I.bb.bam && \
    samtools index $I.bb.bam && \
	touch $I.bb.bam.complete;

    log_depend $I.bb.bam.complete;


    printf "%s\t%d\n" $contig $length >$I.bb.genome && \
    printf "%s\t%d\t%d\n" $contig $start $end >$I.bb.bed && \
    bedtools makewindows -w 50000 -s 49000 -b $I.bb.bed | \
    perl -ane 'print("$F[0]\t$F[1]\t$F[2]\t$F[0]/0/$F[1]_$F[2]\n")' >| $I.read.bed && \
	touch $I.read.bed.complete;

    log_depend $I.read.bed.complete;


    bedtools getfasta -fi $I.bb.fa -bed $I.read.bed -name -tab -fo /dev/stdout | \
    perl -ane "if (\$F[0] =~ /^(\S+)\/0\/(\d+)_(\d+)/) { (\$contig,\$s,\$e) = (\$1,\$2,\$3); \$l=(\$e-\$s); printf(\"\$F[0] \$l \$s \$e +  \$contig $length \$s \$e + %s \$l 0 0 0 254 %s %s %s\\n\",-5*\$l,uc(\$F[1]),\"|\" x length(\$F[1]),uc(\$F[1]));}" >| $I.tmp.m5 && \
    bamtom5 $I.bb.bam $I.bb.fa $I.bb.bed | cat $I.tmp.m5 - | sort -T ${TMPDIR}/${BASE} -t ' ' -k9,9n >| $I.cns.m5 && \
	touch $I.cns.m5.complete;

    log_depend $I.cns.m5.complete;


    log_info "Generating coverage for $contig:$start-$end\n";
    # samtools mpileup -B -q 0 -Q 0 -R -r "$contig:$start-$end" -f ${REF} $I.bb.bam 2>mpileup.log | cut -f1,2,4 >| dp/$I.tmp.dp && \
    bedtools bamtobed -i $I.bb.bam | bedtools genomecov -bga -g $I.bb.genome -i - 2>genomecov.log >| dp/$I.dp.tmp && \
    mv dp/$I.dp.tmp dp/$I.dp && \
	touch $I.dp.complete || \
	cat genomecov.log;

    log_depend $I.dp.complete;


    log_info "Generating consensus for $contig:$start-$end\n";

    ## pbdagcon:
    pbdagcon ${CONSENSUS_EXE_OPTIONS} $I.cns.m5 >cns/$I.fa.tmp && \
    mv cns/$I.fa.tmp cns/$I.cns.fa && \
	touch $I.cns.fa.complete;

    if [[ -e "$I.cns.fa.complete" ]]; then
	log_info "Consensus success for $contig:$start-$end\n";
    else
	log_info "Consensus failure for $contig:$start-$end\n";
    fi

    i=$(( $i + 1 ));
done


log_info "Combining files into $OUT\n";
cat cns/*.cns.fa | tr '/' ' ' | fold -w100 >| ${OUT}.fasta;
#cat dp/*.dp | sed 's/\t/:/' >| ${OUT}.depth;
cat dp/*.dp >| ${OUT}.depth;

cd ${TMPDIR} && rm -r ${TMPDIR}/${BASE}

log_info "Finished\n";

