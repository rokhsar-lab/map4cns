
AUTHORS='Jessen V. Bredeson';
PACKAGE='__PACKAGE_NAME__';
CONTACT='__PACKAGE_CONTACT__';
VERSION='__PACKAGE_VERSION__';
PROGRAM=$(basename $0 .sh);
PURPOSE='Map reads and perform DAG consensus';

function log_info () { 
    printf "%s %s INFO  [%s] $1" `date '+%F %H:%M:%S,%3N'` ${PROGRAM} >>/dev/fd/2;
}

function log_warn () { 
    printf "%s %s WARN  [%s] $1" `date '+%F %H:%M:%S,%3N'` ${PROGRAM} >>/dev/fd/2;
}

function log_error () {
    printf "%s %s ERROR [%s] $2.\n\n" `date '+%F %H:%M:%S,%3N'` ${PROGRAM} >>/dev/fd/2;
    exit $1;
}

function error () {
    printf "Error: $2.\n\n" >>/dev/fd/2;
    exit $1;
}

function log_depend () { 
    if [[ ! -e "${WORKDIR}/$1.complete" ]]; then
        log_error 1 "Missing dependency ($1), stage did not complete";
    fi
}

function abspath () {
    if [[ -z "$1" ]]; then
        log_error 255 "BUG: No var passed to abspath()";
    fi

    var=\$"$1";
    val=`eval "expr \"$var\" "`;
    if [[ -z "$val" ]]; then
        log_error 255 "Bad path, path is empty"

    elif [[ `echo "$val" | grep '^\/'` ]]; then
	# already absolute
        eval "$1=\"$val\"";
    
    else
        if [[ "$val" == "." || "$val" == "./" ]]; then
            eval "$1=\"$PWD\"";

        elif [[ `echo "$val" | grep '^\.\/'` ]]; then
            val=`echo "$val" | sed 's/^\.\/*//'`;
            eval "$1=\"$PWD/$val\"";

        else
            eval "$1=\"$PWD/$val\"";
        
        fi
    fi
}

function validateFOFN () {
    if [[ -z "$1" ]]; then
	log_error 1 "No FOFN given";
	
    elif [[ ${READS_PAIRED} && ${READS_COLLATED} ]]; then
	${GREP} -v '^#' $1 | while read f; do 
	    if   [[ -z "$f" || ! -f "$f" ]]; then
		log_error 1 "$1 collated PE file not defined or does not exist: $f";
	    fi
	done
    
    elif [[ ${READS_PAIRED} ]]; then
	${GREP} -v '^#' $1 | while read f r; do 
	    if   [[ -z "$f" || ! -f "$f" ]]; then
		log_error 1 "$1 uncollated PE 1 file not defined or does not exist: $f";
	    elif [[ -z "$r" || ! -f "$r" ]]; then
		log_error 1 "$1 uncollated PE 2 file not defined or does not exist: $r";
	    fi
	done
    else
	${GREP} -v '^#' $1 | while read f; do 
	    if   [[ -z "$f" || ! -f "$f" ]]; then
		log_error 1 "$1 SR file not defined or does not exist: $f";
	    fi
	done	
    fi
}

function usage () {
    printf "\n" >>/dev/fd/2;
    printf "Program: $PROGRAM ($PURPOSE)\n" >>/dev/fd/2;
    printf "Version: $PACKAGE $VERSION\n" >>/dev/fd/2;
    printf "Contact: $CONTACT\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Usage:   %s --config <config> [--stop-after <stage>]\n" ${PROGRAM} >>/dev/fd/2;
    printf "                [--redo-from <stage>] [--cleanup]\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
}

function usage_short () {
    usage;
    printf "Notes:\n\n" >>/dev/fd/2;
    printf "    Use \`%s --help\` for more information about the above\n" ${PROGRAM} >>/dev/fd/2;
    printf "    options.\n\n\n" >>/dev/fd/2;
}

function usage_long () {
    # TODO: fill out long-option information
    usage;
    printf "Command-line arguments:\n\n" >>/dev/fd/2;
    printf "    --config <config>\n" >>/dev/fd/2;
    printf "       File; required. The configuration file parameterizing a map4cns run.\n" >>/dev/fd/2;
    printf "       The config specifies, at a minimum, the BACKBONE_FASTA, READS_FOFN,\n" >>/dev/fd/2;
    printf "       and WORKDIR variables. See \`examples/map4cns.config\' for details.\n" >>/dev/fd/2;
    printf "       Default: Null\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --redo-from <stage>\n" >>/dev/fd/2;
    printf "       String; optional. Force redo all stages starting from (and including)\n" >>/dev/fd/2;
    printf "       the specified stage. Valid stages are: faidx, saidx, bwtidx, blasr,\n" >>/dev/fd/2;
    printf "       bwa, sort, merge, stats, balance, consensus, gather\n" >>/dev/fd/2;
    printf "       Default: Null\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --stop-after <stage>\n" >>/dev/fd/2;
    printf "       String; optional. Stop the pipeline after the specified stage is\n" >>/dev/fd/2;
    printf "       completed. See \`--redo-from\' option for a list of valid stages.\n" >>/dev/fd/2;
    printf "       Default: Null\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "    --cleanup\n" >>/dev/fd/2;
    printf "       Flag; optional. Remove intermediate files in completed project stage\n" >>/dev/fd/2; 
    printf "       subdirectories.\n" >>/dev/fd/2;
    printf "       Default: false\n"  >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
}


# ==================== SYSTEM =====================
_SH=`which bash`;  # force bash over the cluster 
_OS=`uname`;

if [[ ${_OS} == 'Darwin' ]]; then
    _HOST=`hostname`;
else
    _HOST=`hostname --fqdn`;
fi


# ==================== PACKAGE ====================
PKG_PATH=`dirname  "$0"`;
PKG_PATH=`dirname  "$PKG_PATH"`;
PKG_BASE=`basename "$0"`;

# ==================== RUNTIME ====================
PREFIX=;
WORKDIR=;
BACKBONE_FASTA=;
READS_FOFN=;
READS_COLLATED=;
READS_PAIRED=;

CURRSTAGE=;
PREVSTAGE=;
REDOSTAGE='';
STOPSTAGE='';
CLEANUP=;
CLEANUP_STAGES="gather consensus balance stats merge sort filter bwa blasr bwtidx saidx faidx"
HELP_MESSAGE=;
COMMANDLINE="${PKG_BASE} $*";
while [[ -n $@ ]]; do
    case "$1" in
	'--config') shift; CONFIG="$1";;
	'--stop-after') shift; STOPSTAGE="$1";;
	'--redo-from') shift; REDOSTAGE="$1";;
	'--cleanup') CLEANUP=1;;
	'--help') HELP_MESSAGE='long';;
	'-h') HELP_MESSAGE='short';;
	-*) usage_short; error 2 "Invalid option: $1";;
	*) break;;
    esac;
    shift;
done

if [[ -n "${HELP_MESSAGE}" ]]; then
    if [[ ${HELP_MESSAGE} == 'long' ]]; then
	usage_long;
    else
	usage_short;
    fi
    exit 1;
fi

if [[ -z ${CONFIG} || ! -f ${CONFIG} ]]; then
    usage_short;
    error 1 "Argument to '--config' not defined or file does not exist";
fi

for stage in ${REDOSTAGE} ${STOPSTAGE}; do
    case "${stage}" in
	'faidx');;
	'saidx');;
	'bwtidx');;
	'blasr');;
	'filter');;
	'bwa');;
	'sort');;
	'merge');;
	'stats');;
	'balance');;
	'consensus');;
	'gather');;
	*) usage_short; error 1 "Invalid stage name: ${stage}";;
    esac
done


printf "%s %s INFO  [%s] Command-line: ${COMMANDLINE}\n" `date '+%F %H:%M:%S,%3N'` ${PKG_BASE} >>/dev/fd/2;
printf "%s %s INFO  [%s] Executing %s (v%s) as %s@%s on [%s] %s %s (%s)\n" \
    `date '+%F %H:%M:%S,%3N'` ${PKG_BASE} ${PKG_BASE} ${VERSION} ${USER} ${_HOST} \
    `ps -o comm -p ${PPID} | grep -v 'COMM'` `uname -srm` >>/dev/fd/2;


PATH=${PKG_PATH}/bin:${PATH};
PERL5LIB=${PKG_PATH}/lib:${PERL5LIB};
PYTHONPATH=${PKG_PATH}/lib:${PYTHONPATH};
LIBRARY_PATH=${PKG_PATH}/lib:${LIBRARY_PATH};
LD_LIBRARY_PATH=${PKG_PATH}/lib:${LD_LIBRARY_PATH};

# Validate the accessibility of tools and the config:
TR=`which tr`;
AWK=`which awk`;
CAT=`which cat`;
CUT=`which cut`;
GREP=`which grep`;
PBEC=`which pbec`;
TOUCH=`which touch`;
QBATCH=`which qbatch`;
FILTER=`which bamfilter`;
BALANCE=`which balanceFasta`;
SAMTOOLS=`which samtools`;
BEDTOOLS=`which bedtools`; 
PBDAGCON=`which pbdagcon`;
if [[ -z "${TR}" || ! -x "${TR}" ]]; then
    log_error 127 "tr not in PATH env variable or is not executable";

elif [[ -z "${AWK}" || ! -x "${AWK}" ]]; then
    log_error 127 "awk not in PATH env variable or is not executable";

elif [[ -z "${CAT}" || ! -x "${CAT}" ]]; then
    log_error 127 "cat not in PATH env variable or is not executable";

elif [[ -z "${CUT}" || ! -x "${CUT}" ]]; then
    log_error 127 "cut not in PATH env variable or is not executable";

elif [[ -z "${GREP}" || ! -x "${GREP}" ]]; then
    log_error 127 "grep not in PATH env variable or is not executable";

elif [[ -z "${TOUCH}" || ! -x "${TOUCH}" ]]; then
    log_error 127 "touch not in PATH env variable or is not executable";

elif [[ -z "${QBATCH}" || ! -x "${QBATCH}" ]]; then
    log_error 127 "qbatch not in PATH env variable or is not executable";

elif [[ -z "${BEDTOOLS}" || ! -x "${BEDTOOLS}" ]]; then
    log_error 127 "bedtools not in PATH env variable or is not executable";

elif [[ -z "${SAMTOOLS}" || ! -x "${SAMTOOLS}" ]]; then
    log_error 127 "samtools not in PATH env variable or is not executable";

elif [[ -z "${PBDAGCON}" || ! -x "${PBDAGCON}" ]]; then
    log_error 127 "pbdagcon not in PATH env variable or is not executable";
fi

SAMTOOLS_VERSION=`${SAMTOOLS} 2>&1 | ${GREP} ^Version | ${CUT} -f2 -d' ' | ${CUT} -f1,2 -d'.'`;
if [[ -z ${SAMTOOLS_VERSION} || ${SAMTOOLS_VERSION} < 0.2 ]]; then
    log_error 1 "samtools v0.2+ required, detected v${SAMTOOLS_VERSION}";
fi

# TODO: Invest some time in shielding this from malicious and accidental failure:
. ${CONFIG};

if [[ ${READS_COLLATED} ]]; then
    READS_PAIRED=1;
fi
if [[ ${READS_PAIRED} ]]; then
    USE_BWA=1;
fi

if [[ ${USE_BWA} ]]; then
    for stage in ${REDOSTAGE} ${STOPSTAGE}; do
	case "${stage}" in
	    'faidx');;
	    'bwtidx');;
	    'bwa');;
	    'filter');;
	    'sort');;
	    'merge');;
	    'stats');;
	    'balance');;
	    'consensus');;
	    'gather');;
	    *) usage_short; error 1 "Invalid stage name for BWA alignment: ${stage}";;
	esac
    done

    BWA=`which bwa`;
    if [[ -z "${BWA}" || ! -x "${BWA}" ]]; then
	log_error 127 "bwa not in PATH env variable or is not executable";
    fi
    BWA_VERSION=`${BWA} 2>&1 | ${GREP} ^Version | ${CUT} -f2 -d' ' | ${CUT} -f1,2 -d'.'`;
    if [[ ${BWA_VERSION} < 0.7 ]]; then
	log_error 1 "bwa v0.7+ required, detected v${BWA_VERSION}";
    fi
    BWA=bwa;
else
    for stage in ${REDOSTAGE} ${STOPSTAGE}; do
	case "${stage}" in
	    'faidx');;
	    'saidx');;
	    'blasr');;
	    'filter');;
	    'sort');;
	    'merge');;
	    'stats');;
	    'balance');;
	    'consensus');;
	    'gather');;
	    *) usage_short; error 1 "Invalid stage name for Blasr alignment: ${stage}";;
	esac
    done

    BLASR=`which blasr`;
    SAWRITER=`which sawriter`;
    if [[ -z "${BLASR}" || ! -x "${BLASR}" ]]; then
	log_error 127 "blasr not in PATH env variable or is not executable";
    fi
    if [[ -z "${SAWRITER}" || ! -x "${SAWRITER}" ]]; then
	log_error 127 "sawriter not in PATH env variable or is not executable";
    fi    
    BLASR_VERSION=`${BLASR} --version 2>&1 | grep ^blasr | cut -f2 | cut -f1,2 -d'.'`;
    if [[ -z ${BLASR_VERSION} ]]; then
	BLASR_VERSION=`${BLASR} -version 2>&1 | grep ^blasr | cut -f2 | cut -f1,2 -d'.'`;
    fi
    
    if [[ -z ${BLASR_VERSION} || ${BLASR_VERSION} < 5.1 ]]; then
	log_error 1 "blasr v5.1+ required, detected v${BLASR_VERSION}";
    fi
    SAWRITER=sawriter;
    BLASR=blasr;
fi
SAMTOOLS=samtools;
BEDTOOLS=bedtools;
TOUCH=touch;
GREP=grep;
CUT=cut;
CAT=cat;
AWK=awk;
TR=tr;

if [[ -z "${BACKBONE_FASTA}" || ! -f "${BACKBONE_FASTA}" ]]; then
    log_error 1 "Config parameter BACKBONE_FASTA not defined or the file does not exist";

elif [[ -z "${READS_FOFN}" || ! -f "${READS_FOFN}" ]]; then
    log_error 1 "Config parameter READS_FOFN not defined or the file does not exist";

elif [[ -z "${WORKDIR}" ]]; then
    log_error 1 "Config parameter WORKDIR not defined";

fi
if [[ ! ${NUM_PARTS} || ${NUM_PARTS} < 1 ]]; then
    NUM_PARTS=100;
fi

abspath BACKBONE_FASTA;
abspath READS_FOFN;
abspath WORKDIR;

mkdir -p ${WORKDIR};

if [[ -n "${REDOSTAGE}" ]]; then
    stages="";
    if [[ "${REDOSTAGE}" == "faidx" ]]; then
	if [[ ${USE_BWA} ]]; then
	    stages="bwa bwtidx";
	else
	    stages="filter blasr saidx";
	fi
    fi
    case "${REDOSTAGE}" in
	'faidx') stages="gather consensus balance stats merge sort $stages faidx";;
	'saidx') stages="gather consensus balance stats merge sort filter blasr saidx";;
	'bwtidx') stages="gather consensus balance stats merge sort bwa bwtidx";;
	'blasr') stages="gather consensus balance stats merge sort filter blasr";;
	'bwa') stages="gather consensus balance stats merge sort bwa";;
	'filter') stages="gather consensus balance stats merge sort filter";;
	'sort') stages="gather consensus balance stats merge sort";;
	'merge') stages="gather consensus balance stats merge";;
	'stats') stages="gather consensus balance stats";;
	'balance') stages="gather consensus balance";;
	'consensus') stages="gather consensus";;
	'gather') stages="gather";;
    esac
    if [[ -n "${stages}" && -e "${WORKDIR}.complete" ]]; then
	rm "${WORKDIR}.complete";
    fi
    for stage in $stages; do
	if [[ -e "${WORKDIR}/${stage}.complete" ]]; then
	    rm "${WORKDIR}/${stage}.complete";
	fi
    done
fi


if [[ ! -s "${WORKDIR}/reads.fofn" ]]; then
    validateFOFN ${READS_FOFN};
    mkdir -p ${WORKDIR}/reads;
    printf '' >${WORKDIR}/.reads.fofn;
    ${GREP} -v '^#' ${READS_FOFN} | while read file1 file2; do 
	base1=`basename $file1`;
	abspath file1;
	ln -sf $file1 ${WORKDIR}/reads/$base1;
	
	if [[ ${READS_PAIRED} && ! ${READS_COLLATED} && -n "$file2" ]]; then
	    base2=`basename $file2`;
	    abspath file2;
	    ln -sf $file2 ${WORKDIR}/reads/$base2;
	    echo "${WORKDIR}/reads/$base1 ${WORKDIR}/reads/$base2" >>"${WORKDIR}/.reads.fofn";
	else
	    echo "${WORKDIR}/reads/$base1" >>"${WORKDIR}/.reads.fofn";
	fi
    done
    validateFOFN "${WORKDIR}/.reads.fofn" && \
	mv "${WORKDIR}/.reads.fofn" "${WORKDIR}/reads.fofn";
fi

if [[ ! -s "${WORKDIR}/reads.fofn" ]]; then
    log_error 1 "Encountered an error localizing read files";
fi
READS_FOFN="${WORKDIR}/reads.fofn";
PREFIX=`basename "${WORKDIR}"`;

echo "export PATH=\"${PATH}:\${PATH}\";" >${WORKDIR}/.map4cns_profile;
echo "export INCLUDE_PATH=\"${INCLUDE_PATH}:\${INCLUDE_PATH}\";" >>${WORKDIR}/.map4cns_profile;
echo "export LIBRARY_PATH=\"${LIBRARY_PATH}:\${LIBRARY_PATH}\";" >>${WORKDIR}/.map4cns_profile;
echo "export LD_LIBRARY_PATH=\"${LD_LIBRARY_PATH}:\${LD_LIBRARY_PATH}\";" >>${WORKDIR}/.map4cns_profile;
echo "export PYTHONPATH=\"${PYTHONPATH}:\${PYTHONPATH}\";" >>${WORKDIR}/.map4cns_profile;
echo "export PERL5LIB=\"${PERL5LIB}:\${PERL5LIB}\";" >>${WORKDIR}/.map4cns_profile;
echo "export BACKBONE_FASTA=\"${BACKBONE_FASTA}\";" >>${WORKDIR}/.map4cns_profile;
echo "export READS_FOFN=\"${READS_FOFN}\";" >>${WORKDIR}/.map4cns_profile;
echo "export WORKDIR=\"${WORKDIR}\";" >>${WORKDIR}/.map4cns_profile;
echo "export READS_COLLATED=${READS_COLLATED};" >>${WORKDIR}/.map4cns_profile;
echo "export READS_PAIRED=${READS_PAIRED};" >>${WORKDIR}/.map4cns_profile;
echo "export NUM_PARTS=${NUM_PARTS};" >>${WORKDIR}/.map4cns_profile;
echo "export USE_BWA=${USE_BWA};" >>${WORKDIR}/.map4cns_profile;
echo "export BLASR_EXE_OPTIONS=\"${BLASR_EXE_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export BLASR_SUBMIT_OPTIONS=\"${BLASR_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export FILTER_EXE_OPTIONS=\"${FILTER_EXE_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export FILTER_SUBMIT_OPTIONS=\"${FILTER_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export BWA_EXE_OPTIONS=\"${BWA_EXE_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export BWA_SUBMIT_OPTIONS=\"${BWA_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export SAMSORT_EXE_OPTIONS=\"${SAMSORT_EXE_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export SAMSORT_SUBMIT_OPTIONS=\"${SAMSORT_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export SAMMERGE_EXE_OPTIONS=\"${SAMMERGE_EXE_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export SAMMERGE_SUBMIT_OPTIONS=\"${SAMMERGE_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export STATS_SUBMIT_OPTIONS=\"${STATS_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export CONSENSUS_EXE_OPTIONS=\"${CONSENSUS_EXE_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;
echo "export CONSENSUS_SUBMIT_OPTIONS=\"${CONSENSUS_SUBMIT_OPTIONS}\";" >>${WORKDIR}/.map4cns_profile;

BACKBONE_BASE=`basename ${BACKBONE_FASTA} | sed 's/\.f\(ast\|n\|\)a$//'`;
if [[ ! -e "${WORKDIR}/${BACKBONE_BASE}.fasta" ]]; then
    ln -sf "${BACKBONE_FASTA}" "${WORKDIR}/${BACKBONE_BASE}.fasta";
fi
BACKBONE_FASTA="${WORKDIR}/${BACKBONE_BASE}.fasta";

validateFOFN "${READS_FOFN}";

if [[ ${USE_BWA} ]]; then
    if [[ -e "${WORKDIR}.complete" ]]; then
	log_info "Finished\n";
	exit 0;
    fi


    log_info "faidx index: ";
    PREVSTAGE=;
    CURRSTAGE='faidx';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	${SAMTOOLS} faidx ${BACKBONE_FASTA} &>${WORKDIR}/${CURRSTAGE}.log && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi
    
    # create Burrows-Wheeler index (if necessary)
    log_info "BWT index: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='bwtidx';
    BACKBONE_SA="${BACKBONE_FASTA}.bwt";
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	${BWA} index ${BACKBONE_FASTA} &>${WORKDIR}/${CURRSTAGE}.log && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi

    log_info "bwa: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='bwa';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	log_depend 'faidx';
	log_depend 'bwtidx';

	[[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir ${WORKDIR}/${CURRSTAGE};

	SMART_PAIRING='';
	if [[ -n "${READS_COLLATED}" ]]; then
	    SMART_PAIRING='-p';
	fi

	n=0;
	${GREP} -v '^#' ${READS_FOFN} | while read file1; do
	    n=$(( $n + 1 ));
	    N=`printf %05d $n`;
	    # cat ${WORKDIR}/.map4cns_profile >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.sh;
	    # echo "${BWA} mem \${BWA_EXE_OPTIONS} ${SMART_PAIRING} ${BACKBONE_FASTA} ${file1} | ${SAMTOOLS} view -bS - >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam" >>${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.sh;
	    # echo "${_SH} ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.sh";

	    echo ". \"${WORKDIR}/.map4cns_profile\"; ${BWA} mem \${BWA_EXE_OPTIONS} ${SMART_PAIRING} ${BACKBONE_FASTA} ${file1} | ${SAMTOOLS} view -bS - >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam";

	done >${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${BWA_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi

    log_info "sort: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='sort';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	log_depend ${PREVSTAGE};
	
	n=0;
	${GREP} -v '^#' ${READS_FOFN} | while read file1; do 
	    n=$(( $n + 1 ));
	    N=`printf %05d $n`;
	    echo ". \"${WORKDIR}/.map4cns_profile\"; ${SAMTOOLS} sort \${SAMSORT_EXE_OPTIONS} -o ${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}.srt.bam ${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}.bam";
	done >${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${SAMSORT_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;	
    else
	echo "current" >>/dev/fd/2;
    fi
else
    if [[ -e "${WORKDIR}.complete" ]]; then
	log_info "Finished\n";
	exit 0;
    fi    

    log_info "faidx index: ";
    PREVSTAGE=;
    CURRSTAGE='faidx';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	${SAMTOOLS} faidx ${BACKBONE_FASTA} &>${WORKDIR}/${CURRSTAGE}.log && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi

    log_info "SA index: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='saidx';
    BACKBONE_SA=`echo "${BACKBONE_FASTA}" | sed 's/\.f\(ast\)*a$/.sa/'`;
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	${SAWRITER} ${BACKBONE_SA} ${BACKBONE_FASTA} &>${WORKDIR}/${CURRSTAGE}.log && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi

    # If we want to handle illumina data with blasr, we need to convert the IDs 
    # to the PB format: <ID>/<end>/<start>_<end>

    log_info "blasr: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='blasr';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	log_depend 'faidx';
	log_depend 'saidx';

	[[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir ${WORKDIR}/${CURRSTAGE};

	n=0;
	${GREP} -v '^#' ${READS_FOFN} | while read file; do 
	    n=$(( $n + 1 ));
	    N=`printf %05d $n`;
	    echo ". \"${WORKDIR}/.map4cns_profile\"; ${BLASR} ${file} ${BACKBONE_FASTA} --sa ${BACKBONE_SA} \${BLASR_EXE_OPTIONS} --bam --out ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam";
	done >${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${BLASR_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi


    log_info "filter: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='filter';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	log_depend ${PREVSTAGE};
	
	[[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir ${WORKDIR}/${CURRSTAGE};

	n=0;
	${GREP} -v '^#' ${READS_FOFN} | while read file; do
	    n=$(( $n + 1 ));
	    N=`printf %05d $n`;
	    echo ". \"${WORKDIR}/.map4cns_profile\"; ${FILTER} \${FILTER_EXE_OPTIONS} ${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}.bam ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.bam";
	done >${WORKDIR}/${CURRSTAGE}.batch

	${QBATCH} submit ${FILTER_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
            ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
    else
        echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
        log_info "Stop requested after ${CURRSTAGE}\n";
        exit 0;
    fi


    log_info "sort: ";
    PREVSTAGE=${CURRSTAGE};
    CURRSTAGE='sort';
    if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
	echo "executing" >>/dev/fd/2;
	log_depend ${PREVSTAGE};
	
	[[ -d "${WORKDIR}/${CURRSTAGE}" ]] || mkdir ${WORKDIR}/${CURRSTAGE};

	n=0;
	${GREP} -v '^#' ${READS_FOFN} | while read file; do 
	    n=$(( $n + 1 ));
	    N=`printf %05d $n`;
	    echo ". \"${WORKDIR}/.map4cns_profile\"; ${SAMTOOLS} sort \${SAMSORT_EXE_OPTIONS} -o ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.TMP.bam ${WORKDIR}/${PREVSTAGE}/${PREFIX}_${N}.bam && ${SAMTOOLS} calmd -b ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.TMP.bam ${BACKBONE_FASTA} >${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.srt.bam && rm ${WORKDIR}/${CURRSTAGE}/${PREFIX}_${N}.TMP.bam";
	done >${WORKDIR}/${CURRSTAGE}.batch;
	
	${QBATCH} submit ${SAMSORT_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH}  ${WORKDIR}/${CURRSTAGE}.batch && \
	    ${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;	
    else
	echo "current" >>/dev/fd/2;
    fi
    if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
	log_info "Stop requested after ${CURRSTAGE}\n";
	exit 0;
    fi
fi

if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi
BAMDIR="${WORKDIR}/${CURRSTAGE}";


log_info "merge: ";
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='merge';
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    printf ". \"${WORKDIR}/.map4cns_profile\"; " >${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} merge \${SAMMERGE_EXE_OPTIONS} ${WORKDIR}/${PREFIX}.bam ${BAMDIR}/${PREFIX}_?????.srt.bam && " >>${WORKDIR}/${CURRSTAGE}.batch;
    printf "${SAMTOOLS} index ${WORKDIR}/${PREFIX}.bam\n" >>${WORKDIR}/${CURRSTAGE}.batch;

    ${QBATCH} submit ${SAMMERGE_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi

log_info "stats: ";
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='stats';
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then 
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};
    
    mkdir -p ${WORKDIR}/${CURRSTAGE};

    n=0;
    ${GREP} -v '^#' ${READS_FOFN} | while read file; do
        n=$(( $n + 1 ));
        N=`printf %05d $n`;
	echo ". \"${WORKDIR}/.map4cns_profile\"; ${SAMTOOLS} view ${BAMDIR}/${PREFIX}_$N.srt.bam | awk 'BEGIN{OFS=\"\t\";prev=\"\";sum=0} {if (prev != \"\" && \$3 != prev) { print prev,sum; sum=0 } sum += length(\$10); prev=\$3} END{print prev,sum}' >${WORKDIR}/${CURRSTAGE}/${PREFIX}_$N.stats"; 
    done >${WORKDIR}/${CURRSTAGE}.batch

    ${QBATCH} submit ${STATS_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi

# TODO: Enable balancing by aligned bases, not reference bases
log_info "balance: ";
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='balance';
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then 
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};
    
    mkdir -p ${WORKDIR}/${CURRSTAGE}; 

    ${BALANCE} -p ${NUM_PARTS} ${BACKBONE_FASTA} ${WORKDIR}/${CURRSTAGE}/${PREFIX} &>${WORKDIR}/${CURRSTAGE}.log && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;

    # ${CAT} ${BACKBONE_FASTA}.fai ${WORKDIR}/${PREVSTAGE}/${PREFIX}_?????.stats | \
    # 	${BALANCE} ${BALANCE_EXE_OPTIONS} - ${WORKDIR}/${CURRSTAGE}/${PREFIX} &>${WORKDIR}/${CURRSTAGE}.log && \
    # 	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi


log_info "consensus: ";
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='consensus';
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then 
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};
    
    mkdir -p ${WORKDIR}/${CURRSTAGE};

    for bed in ${WORKDIR}/${PREVSTAGE}/${PREFIX}.?????.bed; do
	cns=`basename $bed | sed 's/bed/cns/'`;
	echo ". \"${WORKDIR}/.map4cns_profile\"; ${PBEC} ${WORKDIR}/${PREFIX}.bam ${BACKBONE_FASTA} $bed ${WORKDIR}/${CURRSTAGE}/$cns";
    done >${WORKDIR}/${CURRSTAGE}.batch

    ${QBATCH} submit ${CONSENSUS_SUBMIT_OPTIONS} -n ${CURRSTAGE} -W -e ${_SH} ${WORKDIR}/${CURRSTAGE}.batch && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi
if [[ "${CURRSTAGE}" == "${STOPSTAGE}" ]]; then
    log_info "Stop requested after ${CURRSTAGE}\n";
    exit 0;
fi


log_info "gather: ";
PREVSTAGE=${CURRSTAGE};
CURRSTAGE='gather';
if [[ ! -e "${WORKDIR}/${CURRSTAGE}.complete" ]]; then 
    echo "executing" >>/dev/fd/2;
    log_depend ${PREVSTAGE};

    ${CAT} ${WORKDIR}/${PREVSTAGE}/${PREFIX}.?????.cns.fasta >${WORKDIR}/${PREFIX}.cns.fasta && \
    ${CAT} ${WORKDIR}/${PREVSTAGE}/${PREFIX}.?????.cns.depth >${WORKDIR}/${PREFIX}.cns.depth && \
	${TOUCH} ${WORKDIR}/${CURRSTAGE}.complete;
else
    echo "current" >>/dev/fd/2;
fi


if [[ -e "${WORKDIR}/${PREVSTAGE}.complete" ]]; then 
    ${TOUCH} ${WORKDIR}.complete;
    if [[ ${CLEANUP} ]]; then
	log_info "Removing intermediate files\n";
	for stage in ${CLEANUP_STAGES}; do
	    if [[ -e "${WORKDIR}/${stage}.complete" ]]; then
		rm ${WORKDIR}/${stage}.complete;
	    fi
	    if [[ -d "${WORKDIR}/${stage}" ]]; then
		rm -rf ${WORKDIR}/${stage};
	    fi
	done
    fi
    log_info "Finished\n";
fi


