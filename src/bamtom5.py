
import os
import re
import sys
import pysam
import getopt

__author__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert BAM alignment to BLASR -m5 format'


num = len
CIGAR_PADDING_OPERATOR = (4, 5)
QUERY_LENGTH_OPERATOR = {0, 1, 4, 5, 7, 8}
ALIGNMENT_LENGTH_OPERATOR = {0, 1, 2, 3, 6, 7, 8}
REGION_STRING = re.compile('^([^:]+):(\d+)\-(\d+)$')
PYTHONVERSION = sys.version_info[:2]
if PYTHONVERSION < (3,):
    range = xrange
    import string
    NUCLEOTIDE_COMPLEMENT = string.maketrans('-acgturyswkmbdhvnACGTURYSWKMBDHVN','-tgcaayrwsmkvhdbnTGCAAYRWSMKVHDBN')
else:
    NUCLEOTIDE_COMPLEMENT = {
        45: '-',
        65: 'T',  97: 't',
        66: 'V',  98: 'v',
        67: 'G',  99: 'g',
        68: 'H', 100: 'h',
        71: 'C', 103: 'c',
        72: 'D', 104: 'd',
        75: 'M', 107: 'm',
        77: 'K', 109: 'k',
        78: 'N', 110: 'n',
        82: 'Y', 114: 'y',
        83: 'W', 115: 'w',
        84: 'A', 116: 'a',
        85: 'A', 117: 'a',
        86: 'B', 118: 'b',
        87: 'S', 119: 's',
        89: 'R', 121: 'r',
    }


def reverse_sequence(sequence):
    if sequence is None:
        return None
    return sequence[::-1].translate(NUCLEOTIDE_COMPLEMENT)


def reverse_alnstring(quality):
    if quality is None:
        return None
    return quality[::-1]


def query_5p_padding(record):
    spurlen = 0
    for cigar in record.cigartuples:
        if cigar[0] in CIGAR_PADDING_OPERATOR:
            spurlen += cigar[1]
        else:
            break

    return spurlen


def alnLen(record):
    if record.cigartuples is None:
        return 0

    alignment_length = 0
    for operator, length in record.cigartuples:
        if operator in ALIGNMENT_LENGTH_OPERATOR:
            alignment_length += length

    return alignment_length



def query_length(record):
    query_len = 0
    for operator, length in record.cigartuples:
        if operator in QUERY_LENGTH_OPERATOR:
            query_len += length
    
    return query_len



def bamtom5(m5, bam, fasta, bed, min_identity=0.0, symmetric=False):

    ref_lens = dict(zip(fasta.references, fasta.lengths))
    ref_seq = None

    prev_ref_name = None
    for region in bed:
        ref_name = region[0]
        ref_len  = ref_lens[ref_name]
        
        if region[2]:
            region_query = (region[0],region[1],region[2])
        else:
            region_query = (region[0], 0, ref_len)

        if ref_name != prev_ref_name:
            ref_seq = fasta.fetch(ref_name).upper()
            prev_ref_name = ref_name

        for record in bam.fetch(*region_query):
            num_insert = 0
            num_delete = 0
            num_match  = 0
            num_mismatch = 0
            qry_name = record.query_name
            qry_seq  = record.query_sequence.upper()
            qry_beg  = record.query_alignment_start  # 0-based
            qry_end  = record.query_alignment_end    # 1-based
            ref_beg  = record.reference_start
            ref_end  = record.reference_end
            qry_len  = query_length(record)
            aln_len  = alnLen(record)
            aln_col  = 0
            qry_aln  = ['-'] * aln_len
            ref_aln  = ['-'] * aln_len
            dif_aln  = ['*'] * aln_len
            if record.is_paired:
                qry_name = '%s/%d' % (qry_name, int(record.is_read2)+1)

            # print("%s %s %s %s" % (str(qpos),str(rpos),str(rbase),str('-' if rbase is None else '-' if rbase not in 'ATCGatcg' else str(rbase))))
            for qry_pos, ref_pos in record.get_aligned_pairs():
                # Padded bases have None in pair[1], but so do insertions,
                # we must explicitly check that the range we are iterating
                # over is within the aligned region:
                if qry_pos is not None and ref_pos is not None: # M/=/X
                    qry_aln[aln_col] = qry_seq[qry_pos]
                    ref_aln[aln_col] = ref_seq[ref_pos]
                    if  qry_aln[aln_col] == ref_aln[aln_col]:
                        dif_aln[aln_col] = '|'
                        num_match += 1
                    else:
                        num_mismatch += 1
                    aln_col += 1

                elif qry_pos is None and ref_pos is None:  # N?
                    aln_col += 1
                    
                elif qry_pos is None:  # D
                    ref_aln[aln_col] = ref_seq[ref_pos]
                    aln_col += 1
                    num_delete += 1

                elif ref_pos is None:  # I or S/P
                    if qry_beg <= qry_pos and qry_pos < qry_end:
                        qry_aln[aln_col] = qry_seq[qry_pos]
                        aln_col += 1
                        num_insert += 1
                        
            
            if num_match / float(aln_len) < min_identity:
                continue
                
            score = 10 * num_insert + 10 * num_delete + 6 * num_mismatch - 5 * num_match

            m5.write("%s %d %d %d %s  %s %d %d %d + %d %d %d %d %d %d %s %s %s\n" % (
                qry_name, qry_len, qry_beg, qry_end, '+-'[record.is_reverse],
                ref_name, ref_len, ref_beg, ref_end,
                score, num_match, num_mismatch, num_insert, num_delete,
                record.mapping_quality, 
                ''.join(qry_aln), ''.join(dif_aln), ''.join(ref_aln)
            ))

            if symmetric and ref_name != qry_name:
                if record.is_reverse:
                    qry_beg, qry_end = qry_len - qry_end, qry_len - qry_beg
                    ref_beg, ref_end = ref_len - ref_end, ref_len - ref_beg
                    ref_aln = reverse_sequence(ref_aln)
                    dif_aln = reverse_alnstring(dif_aln)
                    qry_aln = reverse_sequence(qry_aln)

                m5.write("%s %d %d %d %s  %s %d %d %d + %d %d %d %d %d %d %s %s %s\n" % (
                    ref_name, ref_len, ref_beg, ref_end, '+-'[record.is_reverse],
                    qry_name, qry_len, qry_beg, qry_end,
                    score, num_match, num_mismatch, num_delete, num_insert, 
                    record.mapping_quality, 
                    ''.join(ref_aln), ''.join(dif_aln), ''.join(qry_aln),
                ))
                

def readBedFile(bedfile, rod):
    bed = open(bedfile, 'r')
    records = []
    previous_tid = -1
    previous_pos = -1
    for record in bed:
        record = record.rstrip().split('\t')
        
        if len(record) < 3:
            raise Exception("BED record has too few columns: "+str(record))
        
        record[1] = int(record[1])
        record[2] = int(record[2])
        
        current_tid = rod.get_tid(record[0])
        if current_tid < previous_tid:
            raise Exception("BED file not sorted")
        elif current_tid > previous_tid:
            previous_pos = -1
        elif record[2] < record[1]:
            raise Exception("BED record start > stop: " + str(record))
        elif record[1] < previous_pos:
            raise Exception("BED file not sorted")

        records.append(tuple(record))
        previous_tid = current_tid
        previous_pos = record[1]

    bed.close()

    return records



def usage(message=None, status=1):
    message = '' if message is None else "Error: %s\n\n" % message
    sys.stderr.write("\n")
    sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
    sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
    sys.stderr.write("Contact: %s\n" % __contact__)
    sys.stderr.write("\n")
    sys.stderr.write("Usage:   %s [options] <in.bam> <genome.fa> [in.bed]\n" % (
        __program__))
    sys.stderr.write("\n")
    sys.stderr.write("Options: -i <float>  Min identity (fraction) [0.0]\n")
    sys.stderr.write("         -s          Write query-target pairs symmetrically\n")
    sys.stderr.write("         -h,--help   This help message\n")
    sys.stderr.write("\n%s" % message)
    sys.exit(status)



def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'hsi:', ('help',))
    except getopt.GetoptError as err:
        usage(err)

    min_idnt = 0.0
    symmetric = False
    for flag, value in options:
        if   flag in ('-h','--help'): usage()
        elif flag == '-i': min_idnt = float(value)
        elif flag == '-s': symmetric = True

    if num(arguments) < 2:
        usage("Too few arguments")
    elif num(arguments) > 3:
        usage("Too many arguments")

    m5    = sys.stdout
    bam   = pysam.AlignmentFile(arguments[0])
    fasta = pysam.FastaFile(arguments[1])
    bed   = None
    if num(arguments) == 3:
        region = REGION_STRING.match(arguments[2])
        if region:
            bed = [ (region.group(1), int(region.group(2)), int(region.group(3))) ]
        else:
            bed = readBedFile(arguments[2], bam)
    else:
        bed = map(lambda x: (x['SN'], 0, int(x['LN'])), bam.header['SQ'])

    bamtom5(m5, bam, fasta, bed, min_identity=min_idnt, symmetric=symmetric)



main(sys.argv[1:])
