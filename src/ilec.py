
import os
import re
import sys
import pysam
import getopt
import intervals
import logging
import collections

__author__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'ILlumina-based Error Correction'

num = len
PYTHONVERSION = sys.version_info[:2]
if PYTHONVERSION < (3,):
    range = xrange


# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)

def _log_exception(exception, message, traceback):
    """
Exception handler hook to redirect errors to be reported 
by the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)


def calc_Z(x, m, v, n=None):
    if n is not None:
        return float(x - m) / (v/float(n))
    else:
        return float(x - m) / v

    
def calc_Z_prob(Z):
    from scipy.stats import norm
    return norm.cdf([Z]) - norm.cdf([-Z])


def _fold(linear_string, width=None):
    if width is None:
        return linear_string

    folded_string = ''
    string_length = len(linear_string)
    for i in range(int(string_length/width)+1):
        if width*(i+1) < string_length:
            folded_string += linear_string[width*i:width*(i+1)] + "\n"
        else:
            folded_string += linear_string[width*i:string_length]
            
    return folded_string



def format_fastx(record, width=None):
    comment = '' if record.comment is None else ' '+record.comment

    if record.quality is None:
        return ">%s%s\n%s\n" % (record.name, comment, _fold(record.sequence, width=width))
    else:
        return "@%s%s\n%s\n+\n%s\n" % (record.name, comment, record.sequence, record.quality)



def getter(item):
    return item[-1]



def setter(region):
    return (region.beg, region.end, region)

    
class VariantInterval(intervals.Interval):
    def __init__(self, name, beg, end, ref, alt):
        self.name = name
        self.beg = beg
        self.end = end
        self.ref = ref
        self.alt = alt



def usage(message=None, status=1):
    message = '' if message is None else '\nERROR: %s\n\n' % message

    sys.stderr.write("\n")
    sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
    sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
    sys.stderr.write("Contact: %s\n" % __contact__)
    sys.stderr.write("\n")
    sys.stderr.write("Usage:   %s <in.vcf> <in.fasta>\n" % __program__)
    sys.stderr.write("\n")
    sys.stderr.write("Options: -d,--min-depth <uint>  Minimum locus depth for valid patch\n")
    sys.stderr.write("         -D,--max-depth <uint>  Maximum locus depth for valid patch\n")
    sys.stderr.write("         -I,--indels-only       Correct indels only (for CCS/HiFi)\n")
    sys.stderr.write("         -l,--lower-case        Lower-case corrections incorporated\n")
    sys.stderr.write("         -u,--upper-case        Upper-case corrections incorporated\n")
    sys.stderr.write("         -x,--no-exact-match    Ignore requirement that VCF and FASTA\n")
    sys.stderr.write("                                reference alleles must match exactly\n")
    sys.stderr.write("         -X,--debug             Enable logging in debugging mode\n")
    sys.stderr.write("         -h,--help              Output this help message and exit\n")
    sys.stderr.write("\nNote:\n\n")
    sys.stderr.write("  Prior to running, it is highly recommended that the user filter loci\n")
    sys.stderr.write("  with excess low depth or excess high depth.\n")
    sys.stderr.write("\n%s\n" % message)
    sys.exit(status)


    
def main(argv):
    short_options = 'huld:D:IXx'
    long_options = [
        'help',
        'debug',
        'upper-case',
        'lower-case',
        'min-depth=',
        'max-depth=',
        'indels-only',
        'no-exact-match'
    ]
    
    if len(argv) == 0:
        usage()

    try:
        options, arguments = getopt.getopt(argv, short_options, long_options)
    except getopt.GetoptError as error:
        usage(error)

    if len(arguments) != 2:
        usage("Unexpected number of arguments")


    exact = True
    debug = False
    lower = False
    upper = False
    min_depth = 0
    max_depth = sys.maxsize
    indels_only = False
    for flag, value in options:
        if   flag in ('-h','--help'): usage()
        elif flag in ('-X','--debug'): debug = True
        elif flag in ('-d','--min-depth'): min_depth = int(value)
        elif flag in ('-D','--max-depth'): max_depth = int(value)
        elif flag in ('-l','--lower-case'): lower = True
        elif flag in ('-u','--upper-case'): upper = True
        elif flag in ('-I','--indels-only'): indels_only = True
        elif flag in ('-x','--no-exact-match'): exact = False
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        sys.excepthook = _log_exception

    if lower and upper:
        usage('Cannot both upper- and lower-case the corrections')
    elif lower:
        altcase = lambda x: x.lower()
    elif upper:
        altcase = lambda x: x.upper()
    else:
        altcase = lambda x: x
        
    variants_file = pysam.VariantFile(arguments[0],'r')
    rawfasta_file = pysam.FastxFile(arguments[1])
    corfasta_file = sys.stdout

    
    sample_names = list(variants_file.header.samples)
    if num(sample_names) == 0:
        sample_name = None

    elif num(sample_names) > 1:
        logging.error("One (and only one) genotyped sample expected in VCF")
        sys.exit(1)
    else:
        if variants_file.header.formats.get('AD'):
            # GATK and newer versions of FreeBayes use the AD FORMAT field
            def get_allele_depths(sample):
                return sample['AD']

        elif variants_file.header.formats.get('RO'):
            # Old FreeBayes uses RO and AO FORMAT fields for Ref and Alt Observed read counts
            def get_allele_depths(sample):
                if len(sample['AO']) > 1:
                    return (sample['RO'],) + (sample['AO'] )
                else:
                    return (sample['RO'],) + (sample['AO'],)

        else:
            # unspported genotyper
            logging.error("Allele depths required: VCF lacking AD (or RO and AO) fields")
            sys.exit(1)
        
        sample_name = sample_names[0]

    # Filter variant loci to select for genotypes of the type we'd expect
    # to see if the reference contained an error (assuming the variants
    # were called on reads that came from the same sample as the reference):
    #  1. Homozygous differences of the SNV and MNV types
    #  2. Heterozygous, multi-alleleic MNVs

    logging.info("Reading in candidate corrections VCF")

    warn_user = True
    num_del_bases = 0
    num_ins_bases = 0
    num_snv_bases = 0
    num_mnv_bases = 0
    num_del_count = 0
    num_ins_count = 0
    num_snv_count = 0
    num_mnv_count = 0
    num_del_delta = 0
    num_ins_delta = 0
    num_snv_delta = 0
    num_mnv_delta = 0    
    previous_contig = None
    contig_corrections = {}
    INVALID_ALLELES = {'<NON_REF>','*','.','<*>', None}
    HEMIZYGOUS_ALLELES = {'.','*'}
    for variant in variants_file:
        if variant.contig not in contig_corrections:
            # deque.appendleft() is faster than list.insert()
            contig_corrections[variant.contig] = intervals.IntervalList(getter=getter, setter=setter)
            
        if sample_name is None:
            if num(variant.alleles) == 1:
                continue
            if num(variant.alleles) > 2:
                logging.warning("Multi-allelic locus detected, selecting first alt allele")
            try:
                locus_depth = variant.info['DP']
            except KeyError:
                raise KeyError("Loci detected without INFO-DP: PASSing all loci") from None
                    
            if min_depth <= locus_depth <= max_depth:
                allele_chosen = 1
            else:
                continue
            if indels_only and \
               len(variant.alleles[0]) == len(variant.alleles[allele_chosen]):
                # a SNP
                continue
        else:
            genotype = variant.samples[sample_name]

            if INVALID_ALLELES.intersection(set(variant.alleles)):
                # gVCF record
                continue
            elif genotype.allele_indices[0] is None or \
                 genotype.allele_indices[1] is None:
                # no call, filtered genotype, or freebayes-style
                # hemizygous variant (that intersects another locus)
                continue
            elif variant.ref in genotype.alleles:
                # not truly an error in the reference
                continue
            if indels_only and \
               len(genotype.alleles[0]) == len(genotype.alleles[1]):
                # a SNP
                continue
            
            try:
                allele_depths = get_allele_depths(variant.samples[sample_name])
                allele_chosen = max(genotype.allele_indices, key=lambda x: allele_depths[x])    
            except KeyError:
                logging.warn("No Allele Depth field at %s:%d, omitting candidate locus" % (
                    variant.contig, variant.pos))
                continue

            if sum(allele_depths) < min_depth:
                continue
            elif genotype.alleles[0] == genotype.alleles[1]:
                logging.debug("  Hom Alt: %s:%d Alleles=%s GT=%s" % (
                    variant.contig, variant.pos, str(variant.alleles), str(genotype.alleles)))
            else:
                logging.debug("  Het Alt: %s:%d Alleles=%s GT=%s" % (
                    variant.contig, variant.pos, str(variant.alleles), str(genotype.alleles)))

            if sum(allele_depths) > min_depth and allele_depths[0]:
                # Homozygous genotypes can be caused by deep mismapped repeat reads;
                # if the reference allele is found, ignore the variant.
                continue
                
        variant_alt = altcase(variant.alleles[allele_chosen])
        variant_locus = VariantInterval(
            variant.contig,
            variant.pos - 1,
            variant.pos - 1 + len(variant.ref),
            variant.ref.upper(),
            variant_alt
        )
        if contig_corrections[variant.contig].overlap_search(variant_locus) != -1:
                # this variant overlaps another
                logging.debug("  Candidate locus overlaps another locus: %s:%d" % (
                    variant.contig, variant.pos))
                continue

        if len(variant.ref) < len(variant_alt):
            num_ins_delta += len(variant_alt) - len(variant.ref)
            num_ins_bases += len(variant_alt)
            num_ins_count += 1
            
        elif len(variant.ref) > len(variant_alt):
            num_del_delta += len(variant.ref) - len(variant_alt)
            num_del_bases += len(variant_alt)
            num_del_count += 1
            
        elif len(variant.ref) == 1 and \
             len(variant.ref) == len(variant_alt):
            num_snv_bases += 1
            num_snv_count += 1
            
        elif len(variant.ref) == len(variant_alt):
            num_mnv_bases += len(variant_alt)
            num_mnv_count += 1
            
        else:
            raise NotImplementedError("Unhandled variant type: '%s' => '%s'" %(
                variant.ref, variant_alt))

        contig_corrections[variant.contig].add(variant_locus)
        
    variants_file.close()

    logging.info("Total corrections: %d" % (num_ins_count+num_del_count+num_snv_count+num_mnv_count))
    logging.info("                        count        bases        delta")
    logging.info("  Substitutions:")
    logging.info("    Single:      {:>12d} {:>12d} {:>12d}".format(num_snv_count, num_snv_bases, num_snv_delta))
    logging.info("    Multi:       {:>12d} {:>12d} {:>12d}".format(num_mnv_count, num_mnv_bases, num_mnv_delta))
    logging.info("  Insertions:    {:>12d} {:>12d} {:>12d}".format(num_ins_count, num_ins_bases, num_ins_delta))
    logging.info("  Deletions:     {:>12d} {:>12d} {:>12d}".format(num_del_count, num_del_bases, num_del_delta))
    logging.info('')
    logging.info("Applying corrections")
    
    for contig in rawfasta_file:
        contig.quality = None  # fastq => fasta format
        contig.comment = "[%s-%s]" % (__program__, __version__)
        if contig.name in contig_corrections:
            # perform the error correction by just inserting the diff of the
            # ref and alt alleles:
            sequence_end = len(contig.sequence)
            corrections = reversed(contig_corrections[contig.name])            
            corrected_sequence = collections.deque()
            for correction in corrections:
                if exact:
                    assert contig.sequence[correction.beg:correction.end].upper() == correction.ref, \
                        'Reference sequences do not match!'
                
                corrected_sequence.appendleft(contig.sequence[correction.end:sequence_end])
                corrected_sequence.appendleft(correction.alt)

                sequence_end = correction.beg

            corrected_sequence.appendleft(contig.sequence[0:sequence_end])
                
            contig.sequence = ''.join(corrected_sequence)

        corfasta_file.write(format_fastx(contig, width=100))

    corfasta_file.close()
    rawfasta_file.close()

    logging.info("Finished")
    
    return 0


if __name__ == '__main__':
    main(sys.argv[1:])
